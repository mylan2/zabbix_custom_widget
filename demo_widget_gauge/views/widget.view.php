<?php

declare(strict_types=0);

$view = (new CWidgetView($data));
// **
// * 
// Function Find Max a Count
function maxValueInArray($array, $prop)
{
	return max(array_column($array, $prop));
}
// Function Sort Array of Objects by Object Fields in PHP
function sortDesc($a, $b)
{
	return $a->count <=> $b->count;
}
function sortAsc($a, $b)
{
	return $b->count <=> $a->count;
}
// *
// *
// **
// Define variable
$ipTitle = 'ＩＰ';
$requestTitle = 'ＲＥＱＵＥＳＴ';
$urlTitle = 'ＵＲＬ';

// Check Input Data
if ($data) {
	// Convert string data type to JSON data type 
	$myObj = json_decode($data['history']['value']);
	// Option 1: Get Top IP Array from JSON
	$topIP = $myObj->topIP;
	$maxCountTopIP = maxValueInArray($topIP, 'count');
	// Option 2: GET Top URL Array from JSON
	$topURL = $myObj->topURL;
	$maxCountTopURL = maxValueInArray($topURL, 'count');
	// Option 3: GET Top IP URL Array from JSON
	$topIPURL = $myObj->topIPURL;
	$maxCountTopIPURL = maxValueInArray($topIPURL, 'count');
	// 
	$showQuantity = $data['fields_values']['show_value'];
}
// Create a Table View
$table = (new CTable());
// 
$tableHeader = (new CTable());

// Handle
switch ($data['fields_values']['item_option']) {
	// Handle Top IP request with ID: 1
	case 1:
		// Slice Data to $showQuantity value
		if ($showQuantity < count($topIP)) {
			$topIP = array_slice($topIP, 0, $showQuantity);
		}
		// Sort Data
		if ($data['fields_values']['sort_type'] == 1) {
			usort($topIP, 'sortDesc');
		}
		if ($data['fields_values']['sort_type'] == 2) {
			usort($topIP, 'sortAsc');
		}
		// Create table header
		$table->setHeader(array(
			(new CCol())->addClass('ipTitle'),
			(new CCol())->addClass('statusTitle'),
			(new CCol())->addClass('countTitle')
		));
		// 
		$tableHeader->setHeader(array(
			(new CCol($ipTitle))->addClass('ipTitle'), 
			(new CCol())->addClass('statusTitle'),
			(new CCol($requestTitle))
			// ->setAttribute('colspan', '2')
			->addClass('countTitle')
		));
		// Add data into table
		foreach ($topIP as $value) {
			// Get percent value from Number of requests
			$percentStatus = intval($value->count) * 100 / intval($maxCountTopIP) > 0.5 ? intval($value->count) * 100 / intval($maxCountTopIP) : 0.5;
			// Check value is not empty
			if (($value->IP && $value->count) != "") {
				$table->addRow(array(
					(new CCol($value->IP))->addClass('ipClm'),
					// (new CCol())->addClass('statusBarClm')->addStyle("width:" .$percentStatus."%"),
					(new CCol(
						(new CDiv(
							(new CTag('span'))->addClass('statusBar')->addStyle("width:" . $percentStatus . "%")
						))->addClass('statusBarC') 
					)
					)->addClass('statusBarClm'),
					(new CCol(number_format($value->count)))->addClass('countClm')
				));
			}
		}
		break;
	// Handle Top URL request with ID: 2
	case 2:
		// Slice Data to $showQuantity value
		if ($showQuantity < count($topURL)) {
			$topURL = array_slice($topURL, 0, $showQuantity);
		}
		// Sort Data
		if ($data['fields_values']['sort_type'] == 1) {
			usort($topURL, 'sortDesc');
		}
		// Create table header
		$table->setHeader(array(
			(new CCol())->addClass('urlTitle'),
			(new CCol())->addClass('statusTitle'),
			(new CCol())->addClass('countTitle')
		));
		$tableHeader->setHeader(array(
			(new CCol($urlTitle))->addClass('urlTitle'),
			(new CCol())->addClass('statusTitle'),
			(new CCol($requestTitle))->addClass('countTitle')
		));
		// Add data into table
		foreach ($topURL as $value) {
			// Get percent value from Number of requests
			$percentStatus = intval($value->count) * 100 / intval($maxCountTopURL) > 0.5 ? intval($value->count) * 100 / intval($maxCountTopURL) : 0.5;
			// Check value is not empty
			if (($value->URL && $value->count) != "") {
				$table->addRow(array(
					(new CCol($value->URL))->addClass('urlClm'),
					// (new CCol())->addClass('statusBarClm')->addStyle("width:" .$percentStatus."%"),
					(new CCol(
						(new CDiv(
							(new CTag('span'))->addClass('statusBar')->addStyle("width:" . $percentStatus . "%")
						))->addClass('statusBarC') 
					)
					)->addClass('statusBarClm'),
					(new CCol(number_format($value->count)))->addClass('countClm')
				));
			}
		}
		break;
	// Handle Top IP URL request with ID: 3
	case 3:
		// Slice Data to $showQuantity value
		if ($showQuantity < count($topIPURL)) {
			$topIPURL = array_slice($topIPURL, 0, $showQuantity);
		}
		// Sort Data
		if ($data['fields_values']['sort_type'] == 1) {
			usort($topIPURL, 'sortDesc');
		}
		// Create table header
		$table->setHeader(array(
			(new CCol())->addClass('ipTitle'),
			(new CCol())->addClass('urlTitle'),
			(new CCol())->addClass('statusTitle'),
			(new CCol())->addClass('countTitle')
		));
		$tableHeader->setHeader(array(
			(new CCol($ipTitle))->addClass('ipTitle'),
			(new CCol($urlTitle))->addClass('urlTitle'),
			(new CCol())->addClass('statusTitle'),
			(new CCol($requestTitle))->addClass('countTitle')
		));
		// Add data into table
		foreach ($topIPURL as $value) {
			// Get percent value from Number of requests
			$percentStatus = intval($value->count) * 100 / intval($maxCountTopIPURL) > 0.5 ? intval($value->count) * 100 / intval($maxCountTopIPURL) : 0.5;
			// check value is not empty
			if (($value->IP && $value->URL && $value->count) != "") {
				$table->addRow(array(
					(new CCol($value->IP))->addClass('ipClm'),
					(new CCol($value->URL))->addClass('urlClm'),
					// (new CCol())->addClass('statusBarClm')->addStyle("width:" .$percentStatus."%"),
					(new CCol(
						(new CDiv(
							(new CTag('span'))->addClass('statusBar')->addStyle("width:" . $percentStatus . "%")
						))->addClass('statusBarC') 
					)
					)->addClass('statusBarClm'),
					(new CCol(number_format($value->count)))->addClass('countClm')
				));
			}
		}
		break;
	default:
		// No data response
		$table->addRow(array(
			(new CCol('No data'))->addClass(),
		));
		break;
}
$view
	->addItem(
		[
			// Show description
			$data['fields_values']['description']
				? (new CDiv($data['fields_values']['description']))->addClass('description')
				: null,
			// Show table status bar 
			
			(new CDiv($tableHeader->addClass('tblCustomHeader'))),
			(new CDiv($table->addClass('tblCustom')))->addClass('tblContainer')
			
		]
	)
	->setVar('history', $data['history'])
	->setVar('fields_values', $data['fields_values']);
$view->show();

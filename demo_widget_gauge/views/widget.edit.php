<?php declare(strict_types=0);
 
/**
 * Gauge chart widget form view.
 *
 * @var CView $this
 * @var array $data
 */
 

(new CWidgetFormView($data))
	
	->addField(
		new CWidgetFieldMultiSelectItemView($data['fields']['itemid'])
	)
	->addField(
		new CWidgetFieldSelectView($data['fields']['item_option'])
	)
	->addField(
		new CWidgetFieldRadioButtonListView($data['fields']['sort_type'])
	)
	->addField(
		new CWidgetFieldRadioButtonListView($data['fields']['show_value'])
	)
	->addField(
		new CWidgetFieldTextBoxView($data['fields']['description'])
		// 'js-advanced-configuration'
	)
	// ->includeJsFile('widget.edit.js.php')
	// ->addJavaScript('widget_lesson_gauge_chart_form.init('.json_encode([
	// 	'color_palette' => CWidgetFieldGraphDataSet::DEFAULT_COLOR_PALETTE
	// ], JSON_THROW_ON_ERROR).');')
	->show();

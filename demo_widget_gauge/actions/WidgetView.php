<?php declare(strict_types = 0);


namespace Widgets\LessonGaugeChart\Actions;

use API,
	CControllerDashboardWidgetView,
	CControllerResponseData;

class WidgetView extends CControllerDashboardWidgetView {

	protected function doAction(): void {
		$db_items = API::Item()->get([ 
			'itemids' => $this->fields_values['itemid'],
			'webitems' => true,
		]);

		$history_value = null;

		if ($db_items) {
			$item = $db_items[0];

			$history = API::History()->get([
				'output' => API_OUTPUT_EXTEND,
				'itemids' => $item['itemid'],
				'history' => $item['value_type'],
				'sortfield' => 'clock',
				'sortorder' => ZBX_SORT_DOWN,
				'limit' => 1
			]);

			if ($history) {
				$history_value = convertUnitsRaw([
					'value' => $history[0]['value'],
					'units' => $item['units']
				]);
			}
		} 

		$this->setResponse(new CControllerResponseData([
			'name' => $this->getInput('name', $this->widget->getName()),
			'history' => $history_value,
			'fields_values' => $this->fields_values,
			'user' => [
				'debug_mode' => $this->getDebugMode()
			]
		]));
	}
}

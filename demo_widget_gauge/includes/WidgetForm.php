<?php declare(strict_types = 0);

namespace Widgets\LessonGaugeChart\Includes; 

use Zabbix\Widgets\{
	CWidgetField,
	CWidgetForm
};

use Zabbix\Widgets\Fields\{
	CWidgetFieldMultiSelectItem,
	CWidgetFieldRadioButtonList,
	CWidgetFieldSelect,
	CWidgetFieldTextBox
};

/**
 * Gauge chart widget form.
 */
class WidgetForm extends CWidgetForm {
	
	// Item option
	private const TOP_IP = 1; 
	private const TOP_URL = 2; 
	private const TOP_IP_URL = 3; 
	// Sort
	private const TYPE_ASC = 1; 
	private const TYPE_DESC = 2; 
	// ITEM SHOW
	private const SHOW_10 = 10; 
	private const SHOW_30 = 30; 
	private const SHOW_50 = 50; 
	private const SHOW_100 = 100; 

	public function addFields(): self {
		return $this
			
			->addField(
				(new CWidgetFieldMultiSelectItem('itemid', _('Item')))
					->setFlags(CWidgetField::FLAG_NOT_EMPTY | CWidgetField::FLAG_LABEL_ASTERISK)
			)
			->addField(
				(new CWidgetFieldSelect('item_option', _('Item option'), [
					self::TOP_IP => _('TOP IP'),
					self::TOP_URL => _('TOP URL'),
					self::TOP_IP_URL => _('TOP IP URL')
				]))->setDefault(self::TOP_IP)
			)
			->addField(
				(new CWidgetFieldRadioButtonList('sort_type', _('Sort type'), [
					self::TYPE_ASC => _('Asc'),
					self::TYPE_DESC => _('Desc')
				]))->setDefault(self::TYPE_DESC)
			)
			->addField(
				(new CWidgetFieldRadioButtonList('show_value', _('Show value'), [
					self::SHOW_10 => _('10'),
					self::SHOW_30 => _('30'),
					self::SHOW_50 => _('50'),
					self::SHOW_100 => _('100')
				]))->setDefault(self::SHOW_10)
			)
			->addField(
				new CWidgetFieldTextBox('description', _('Description'))
			);
	}
}
